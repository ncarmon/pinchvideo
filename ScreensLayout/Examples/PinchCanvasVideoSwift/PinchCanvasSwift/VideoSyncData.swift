//
//  VideoSyncData
//
//  Created by Nava Carmon on 5/12/20.
//  Copyright © 2020 Jun Tanaka. All rights reserved.
//

import Foundation
import CoreMedia

struct VideoSyncData : Codable {
    var time: CMTime?
    var hostTime: Date?
}
