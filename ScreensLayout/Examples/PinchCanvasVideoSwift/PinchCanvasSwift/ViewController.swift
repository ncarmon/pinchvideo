//
//  ViewController.swift
//  PinchCanvasSwift
//
//  Created by Jun on 12/10/14.
//  Copyright (c) 2014 Jun Tanaka. All rights reserved.
//

import UIKit
import ScreenLayout
import AVFoundation

protocol ControllerDelegate : class {
    func syncWithOtherDevice()
}

class ViewController: SCLPinchViewController {
    
    @IBOutlet weak var videoPlayer: VideoPlayer!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var syncButton: UIButton!

    private let liveURL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let path = Bundle.main.path(forResource: "BigBuckBunny", ofType:"mp4") else {
            debugPrint("BigBuckBunny.mp4 not found")
            return
        }
        let videoURL = URL(fileURLWithPath: path)
        configureVideoPlayer(with: videoURL)
    }
    
    func configureVideoPlayer(with url: URL) {
        videoPlayer.configurePlayer(with: url, autoplay: true, gravity: .resizeAspect)
        videoPlayer.controllerDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.sessionManager.startPeerInvitations(withServiceType: "pinchcanvas", errorHandler: { (error) -> Void in
            print("invitations failed with error: \(String(describing: error))")
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.sessionManager.stopPeerInviations();
    }
    
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    func updateLabels() {
        let numberOfConnectedPeers = self.sessionManager.session.connectedPeers.count
        let numberOfConnectedScreens = SCLScreen.main().connectedScreens.count;
        
        self.textLabel.text = "\(numberOfConnectedScreens) of \(numberOfConnectedPeers) screens connected";
    }
    
    func updateImageFrame() {
        var angle: CGFloat = 0.0
        var frame: CGRect = self.view.bounds
        
        let localScreen = SCLScreen.main()
        if let layout = localScreen.layout {
            // align image rotation to the first screen in the layout
            let originScreen: SCLScreen = layout.screens.first!
            angle = originScreen.convertAngle(0.0, to: self.view)

            // extend image frame to the entire bounds of the layout
            frame = layout.bounds(in: localScreen)
            frame = localScreen.convert(frame, to: self.view)
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.videoPlayer.transform = CGAffineTransform(rotationAngle: angle)
            self.videoPlayer.frame = frame
            
        })
    }
    
    // remote peer changed state
    override func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .notConnected:
            print("peer not connected: \(peerID)")
        case .connecting:
            print("peer connecting: \(peerID)")
        case .connected:
            print("peer connected: \(peerID)")
            videoPlayer.otherPeerWaitingForSync = true
        @unknown default: break
        }
        
        DispatchQueue.main.async {
            self.updateLabels()
        }
    }
    
    // screen layout changed
    override func layoutDidChange(for affectedScreens: [SCLScreen]) {
        DispatchQueue.main.async {
            self.updateLabels()
            self.updateImageFrame()
        }
    }
    
    override func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let videoSyncData = try JSONDecoder().decode(VideoSyncData.self, from: data)
            print("get currentTime: \(String(describing: videoSyncData.time)), hostTime: \(String(describing: videoSyncData.hostTime))")
            if let time = videoSyncData.time, let videoTime = videoPlayer.getCurrentTime(), CMTimeCompare(time, videoTime) > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    self.videoPlayer.videoSyncData = videoSyncData
                }
            }
        } catch {
            fatalError("Unable to process recieved data")
        }

    }
}

extension ViewController : ControllerDelegate {
    func syncWithOtherDevice() {
        let currentTime = videoPlayer.getCurrentTime()
        let hostTime = Date()
        let videoSyncData = VideoSyncData(time: currentTime, hostTime: hostTime)
        let encoder = JSONEncoder.init()
        do {
            let videoData = try encoder.encode(videoSyncData)
            try sessionManager.session.send(videoData, toPeers: sessionManager.session.connectedPeers, with: .reliable)
            print("send currentTime: \(String(describing: currentTime)), hostTime = \(hostTime)")
        } catch {
            fatalError("Could not send videoData")
        }
    }
}



