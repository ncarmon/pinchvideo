//
//  VideoPlayer.swift
//  XaafSampleApp
//
//  Created by Sonia Ziv on 28/02/2019.
//  Copyright © 2019 AT&T. All rights reserved.
//

import UIKit
import AVFoundation

protocol Playable {
    func observeRate(changeHandler: @escaping (Playable, NSKeyValueObservedChange<Float>) -> Void) -> NSKeyValueObservation
}

extension AVPlayer: Playable {
    func observeRate(changeHandler: @escaping (Playable, NSKeyValueObservedChange<Float>) -> Void) -> NSKeyValueObservation {
        return observe(\.rate, options: .new, changeHandler: changeHandler)
    }
}

class VideoPlayer: UIView {
    private var player: AVQueuePlayer?
    private var playerLooper: AVPlayerLooper?
    private weak var container: UIView?

    weak var controllerDelegate: ControllerDelegate?
    
    var autoPlay: Bool = true
    var rateObserver: NSKeyValueObservation?
    var playerItemStatusObserver: NSKeyValueObservation?
    var syncTimer: Timer?
    var otherPeerWaitingForSync: Bool = false {
        didSet {
            if self.isPlaying, otherPeerWaitingForSync {
                DispatchQueue.main.async {
                    self.controllerDelegate?.syncWithOtherDevice()
                    self.otherPeerWaitingForSync = false
                }
            }
        }
    }
    var videoSyncData: VideoSyncData? = nil {
        didSet {
            if self.isPlaying, let videoData = videoSyncData {
                self.synchronizePlayer(videoData)
            } else if let videoData = videoSyncData {
                if syncTimer == nil {
                    DispatchQueue.main.async {
                        self.syncTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
                            self.synchronizePlayer(videoData)
                        }
                    }
                }
            }
        }
    }
    
    var playerItem: AVPlayerItem? = nil {
        willSet {
            playerItemStatusObserver = nil
        }
        didSet {
            playerItemStatusObserver = playerItem?.observe(\.status, options: .new) { [weak self] playerItem, _ in
                guard let self = self, playerItem === self.playerItem else {
                    return
                }
                
                switch playerItem.status {
                case .readyToPlay:
                    print("Player is readyToPlay")
                    self.createRateObserver()
                case .failed:
                    let playerError = playerItem.error
                    print("error = \(String(describing: playerError))")
                default: break
                }
                
                
            }
            // If `isLoopingEnabled` is called before the AVPlayer was set
            if loop {
                configureForLooping(with: playerItem)
            }

        }
    }

    
    var isPlaying: Bool {
        guard let player = player else {
            print("Ther is no player")
            return false
        }
        return player.status == .readyToPlay && player.rate > 0
    }
    
    var loop = true {
        didSet {
            if loop {
                configureForLooping()
            } else {
                stopLooping()
            }
        }
    }
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    func synchronizePlayer(_ videoData: VideoSyncData) {
        guard isPlaying else {
            print("player is not ready to play")
            return
        }
        if syncTimer != nil {
            syncTimer?.invalidate()
            syncTimer = nil
        }
        DispatchQueue.main.async {
            print("set time to \(String(describing: videoData.time))")
            self.setTime(time: videoData.time, hostTime: videoData.hostTime)
        }
    }
    
    func configurePlayer(with url: URL, autoplay: Bool, gravity: AVLayerVideoGravity = .resizeAspect) {
        let asset = AVAsset(url: url)
        let avPlayerItem = AVPlayerItem(asset: asset)
        playerItem = avPlayerItem
        player = AVQueuePlayer(playerItem: playerItem)
        autoPlay = autoplay

        let playerLayer = self.layer as! AVPlayerLayer //swiftlint:disable:this force_cast
        playerLayer.player = player
        playerLayer.videoGravity = gravity
        playerLayer.needsDisplayOnBoundsChange = true
        self.loop = true

        if autoPlay {
            player?.play()
        }
    }
    
    private func createRateObserver() {
        guard let player = player else {
            print("There is not player")
            return
        }
        rateObserver = player.observeRate { [weak self] _, _ in
            guard let self = self, player.rate > 0 else { return }
            
            DispatchQueue.main.async {
                print("Player is playing")
                if self.otherPeerWaitingForSync {
                    self.controllerDelegate?.syncWithOtherDevice()
                    print("Sync data was sent")
                    self.otherPeerWaitingForSync = false
                }
//                if let videoData = self.videoSyncData {
//                    self.setTime(time: videoData.time, hostTime: videoData.hostTime)
//                    print("time was set")
//                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        stopLooping()
        if let avPlayerLayer = container?.layer.sublayers?.first {
            avPlayerLayer.removeFromSuperlayer()
        }
    }
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func muteAudio(_ mute: Bool) {
        player?.isMuted = mute
    }
    
    func getCurrentTime() -> CMTime? {
        return player?.currentTime()
    }
    
    func setTime(time: CMTime?, hostTime: Date?) {
        guard   let time = time,
                let player = player,
                let hostTime = hostTime,
                player.status == .readyToPlay else {
            print("time is nil")
            return
        }
        //DispatchQueue.main.async {
        player.automaticallyWaitsToMinimizeStalling = false
        player.setRate(0.0, time: player.currentTime(), atHostTime: .invalid)
        player.preroll(atRate: 1.0, completionHandler: { (successPreroll) in
            let currentDate = Date()
            let duration = currentDate - hostTime + 0.1
            let newTime = CMTimeMakeWithSeconds(CMTimeGetSeconds(time) + duration, preferredTimescale: player.currentTime().timescale);
            player.setRate(1.0, time: newTime, atHostTime: .invalid)
            print("duration = \(duration), currentDate = \(currentDate)")
            self.videoSyncData = nil
            print("Video is synchronized successPreroll: \(successPreroll)")
        })

    }
    
    func getDateDiff(start: Date, end: Date) -> Int  {
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([Calendar.Component.second], from: start, to: end)

        let seconds = dateComponents.second
        return Int(seconds!)
    }

    
    //var playItemEndObserver: NSObjectProtocol?
    private func configureForLooping(with playerItem: AVPlayerItem? = nil) {
        guard let player = player, let playerItem = playerItem ?? player.currentItem else {
            return
        }
        // Create a new player looper with the queue player and template item
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem)
    }
    
    private func stopLooping() {
        playerLooper?.disableLooping()
    }
}

extension Date {

    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }

}


